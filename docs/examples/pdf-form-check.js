var msg = "";
try {
	var doc = new PDFDocument(scriptArgs[0]);
	var AcroForm = doc.getTrailer().Root.AcroForm;
	if (AcroForm) {
		var hasFields = (AcroForm.Fields && AcroForm.Fields.length > 0);
		var hasXFA = AcroForm.XFA;
		if (hasFields) msg += "AcroForm";
		if (hasFields && hasXFA) msg += ",";
		if (hasXFA) msg += "XFA";
	}
} catch (ex) {
	msg += "Error";
}
print(scriptArgs[0] + "{" + msg + "}");
