/*
 * 
 */
var App = {

    HOMEPAGE : "index.html",
    ARTIFEX : "https://artifex.com",
    RELATIVE_ROOT_PATH : "",

    /** 
     *
     */
    isIETenOrEarlier:function() {
        var ua = window.navigator.userAgent;
        var lowIE = false;
        var msie = ua.indexOf('MSIE ');

        if (msie > 0) {// IE 10 or older => return version number        
            lowIE = true;
        }

        var trident = ua.indexOf('Trident/');
        if (trident > 0) {// IE 11 => return version number        
            lowIE = true;
        }
        
        return lowIE;
    },   

    /** 
     *
     */
    init:function() {

        if(!App.isIETenOrEarlier()) {
            App.loadHeaderNavFooter();
        } else {
            document.location.replace("upgrade-browser.html");
        }

    },

    /** 
     *
     */
    loadHeaderNavFooter:function() {
        var body = document.getElementsByTagName("body")[0];
        App.RELATIVE_ROOT_PATH = body.dataset["value"];

        const promise = new Promise((resolve, reject) => {
        fetch(App.RELATIVE_ROOT_PATH+'json/HeaderNavFooter.json')
            .then(respond => {
                resolve(respond.json());
            }).catch(err => {
                reject(err);
            })
        })

        promise.then((data) => parse(data));

        function parse(data) {
            Header.render(data["Header"]);
            Nav.render(data["Nav"]);
            Footer.render(data["Footer"]);

            App.displayPage();

            // some pages may require settings, here is where we inject that
            App.loadSettings();
        }
    },

    /**
     * 
     */
    displayPage:function() {
        var body = document.getElementsByTagName("main")[0];
        body.style.display = "block";
    },

    /**
     * 
     */
    showPanel:function(obj) {
        var panelDiv = obj.nextSibling.nextSibling;

        if (panelDiv.className === "panel") {

            if (panelDiv.style.display === "block") {
                panelDiv.style.display = "none";
            } else {
                panelDiv.style.display = "block";
            }
        }
    },

    /** 
     *
     */
    loadSettings:function() {
        var body = document.getElementsByTagName("body")[0];
        App.RELATIVE_ROOT_PATH = body.dataset["value"];

        const promise = new Promise((resolve, reject) => {
        fetch(App.RELATIVE_ROOT_PATH+'json/settings.json')
            .then(respond => {
                resolve(respond.json());
            }).catch(err => {
                reject(err);
            })
        })

        promise.then((data) => parse(data));

        function parse(data) {
            settingsLoaded(data);
        }
    }

}

var Header = {

    /**
     *
     */
    render:function(data) {
        ui_str = "";

        var leftLogo = data["left"]["logo"];
        var leftLogoSrc = data["left"]["src"];
        if (leftLogo && leftLogoSrc) {
            ui_str = "<a href='"+App.RELATIVE_ROOT_PATH+App.HOMEPAGE+"'><img class='logo "+leftLogo+"' alt='logo "+leftLogo+"' src='"+App.RELATIVE_ROOT_PATH+leftLogoSrc+"' width=100% /></a>";
        }

        ui_str += "<div class='right'>";

        var rightLogo = data["right"]["logo"];
        var rightLogoSrc = data["right"]["src"];
        if (rightLogo && rightLogoSrc) {
            ui_str += "<a href='"+App.ARTIFEX+"'><img class='logo "+rightLogo+"' alt='logo "+rightLogo+"' src='"+App.RELATIVE_ROOT_PATH+rightLogoSrc+"' width=100% /></a>";
        }

        var buttonA = data["right"]["buttonA"];
        if (buttonA) {

            var external = buttonA["data"]["external"];

            if (external) {
                ui_str += "<a href='"+buttonA["data"]["link"]+"' target=_blank aria-label='"+buttonA["data"]["name"]+"'><button class='buttonA'>"+buttonA["data"]["name"]+"</button></a>";
            } else {
                ui_str += "<a href='"+App.RELATIVE_ROOT_PATH+buttonA["data"]["link"]+"' aria-label='"+buttonA["data"]["name"]+"'><button class='buttonA'>"+buttonA["data"]["name"]+"</button></a>";
            }  
        }

        var buttonB = data["right"]["buttonB"];
        if (buttonB) {

            var external = buttonB["data"]["external"];

            if (external) {
                ui_str += "<a href='"+buttonB["data"]["link"]+"' target=_blank aria-label='"+buttonB["data"]["name"]+"'><button class='buttonB'>"+buttonB["data"]["name"]+"</button></a>";
            } else {
                ui_str += "<a href='"+App.RELATIVE_ROOT_PATH+buttonB["data"]["link"]+"' aria-label='"+buttonB["data"]["name"]+"'><button class='buttonB'>"+buttonB["data"]["name"]+"</button></a>";
            }

        }

        ui_str += "</div>";

        var headerAsseets = document.getElementById("headerAssets");
        headerAsseets.innerHTML = ui_str;
    }

}

var Nav = {

    /**
     *
     */
    render:function(data) {
        var ui_str = "";
        var index = 0;

        for (section in data) {

            var link = ""

            if (section.toLowerCase() == "product") {
                link = "index.html";
            } else if (section.toLowerCase() == "licensing") {
                link = "licensing/index.html";
            } else if (section.toLowerCase() == "documentation") {
                link = "docs/index.html";
            } else if (section.toLowerCase() == "resources") {
                link = "resources/index.html";
            }

            ui_str += "<div class='folder'><a class='item container' href='"+App.RELATIVE_ROOT_PATH+link+"'>"+section+"</a>";

            var folder = data[section];

            if (folder) {

                ui_str += "<div class='content'>";

                for (item in folder) {

                    var link = folder[item].data["link"];
                    var name = folder[item].data["name"];
                    var icon = folder[item].data["icon"];
                    var external = folder[item].data["external"];
                    var externalSameTab = folder[item].data["externalSameTab"];
                    var internalInNewTab = folder[item].data["internalInNewTab"];

                    if (external) {
                        ui_str += "<div class='icon "+icon+"'><a class='item' href='"+link+"' target='mupdf_"+name+"'>"+name+"</a></div>";
                    } else if (externalSameTab) {
                        ui_str += "<div class='icon "+icon+"'><a class='item' href='"+link+"'>"+name+"</a></div>";
                    } else if (internalInNewTab) {
                        ui_str += "<div class='icon "+icon+"'><a class='item' href='"+App.RELATIVE_ROOT_PATH+link+"' target=_blank>"+name+"</a></div>";
                    } else {
                        ui_str += "<div class='icon "+icon+"'><a class='item' href='"+App.RELATIVE_ROOT_PATH+link+"'>"+name+"</a></div>";
                    }  

                }

                ui_str += "</div>"; // close .content

            }

            ui_str += "</div>"; // close .folder

            ui_str += "<div class='spacer'></div>";

            index += 1;
        }

        var nav = document.getElementsByTagName("nav")[0];
        nav.innerHTML = "<div class='desktop'>"+ui_str+"</div>" + "<div class='mobile' id='mobileNav'><div class='burger' id='burger-icon' onClick='Nav.openBurgerMenu()'></div><div class='menu' id='burger-menu'>"+ui_str+"</div></div>";
    },

    openBurgerMenu:function() {

        // switch on
        if (document.getElementById("burger-menu").style.display != "block") {
            document.getElementById("burger-menu").style.display = "block";
            document.getElementById("burger-icon").classList.add("open");
            document.getElementById("mobileNav").classList.add("open");
        } else { // switch off
            document.getElementById("burger-menu").style.display = "none";
            document.getElementById("burger-icon").classList.remove("open");
            document.getElementById("mobileNav").classList.remove("open");
        }
        
    },

    closeBurgerMenu:function() {
        
    }
}

var Footer = {

    /**
     *
     */
    render:function(data) {
        
        var navigation = data["links"];

        var navg_str = "";

        for (item in navigation) {
            var link = navigation[item].data["link"];
            var name = navigation[item].data["name"];
            var external = navigation[item].data["external"];

            if (external) {
                navg_str += "<a class='item' href='"+link+"'>"+name+"</a>";
            } else {
                navg_str += "<a class='item' href='"+App.RELATIVE_ROOT_PATH+link+"'>"+name+"</a>";
            }
            
        }

        var copyrightInfo = data["copyright"];

        var ui_str = "<div class='copyright'><div class='details'>";

        for (item in copyrightInfo) {
            var logo = copyrightInfo[item].data["logo"];
            var src = copyrightInfo[item].data["src"];
            var text = copyrightInfo[item].data["text"];

            if (logo && src) {
                ui_str += "<a href='"+App.ARTIFEX+"'><img class='logo "+logo+"' alt='logo "+logo+"' src='"+App.RELATIVE_ROOT_PATH+src+"' width=100% /></a>";
            }

            if (text) {
                ui_str += "<div class='text'>"+text+"</div>";
            }
        }

        ui_str += "</div></div>";

        var footer = document.getElementsByTagName("footer")[0];
        footer.innerHTML = "<div class='inner'>"+navg_str+ui_str+"</div>";


    }
}


App.init();


// override this on a per page basis
function settingsLoaded(data) {

}













